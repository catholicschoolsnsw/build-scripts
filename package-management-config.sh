#!/bin/bash

function connect_to_azure_artifact_feed {

  echo "Connecting to Azure Artifact Feed";

  if [ -z "${NUGET_SOURCE_USERNAME}" ]; then
    echo "ERROR: NUGET SOURCE USERNAME is not defined, Please set environment variable NUGET_SOURCE_USERNAME";
    exit 1;
  fi

  if [ -z "${NUGET_SOURCE_PASSWORD}" ]; then
    echo "ERROR: NUGET SOURCE PASSWORD is not defined, Please set environment variable NUGET_SOURCE_PASSWORD";
    exit 1;
  fi

  if [ -z "${NUGET_SOURCE_URL}" ]; then
    echo "ERROR: NUGET SOURCE URL is not defined, Please set environment variable NUGET_SOURCE_URL";
    exit 1;
  fi
  
  echo "> Connect to Azure Artifact Feed"
  dotnet nuget add source $NUGET_SOURCE_URL -n artifacts -u $NUGET_SOURCE_USERNAME -p $NUGET_SOURCE_PASSWORD --store-password-in-clear-text

  if [ ! $? -eq 0 ]; then
    echo "ERROR! Unable to authenticate to Azure Artifact Feed";
    exit 1;
  fi
}

function publish_to_azure_artifact_feed_nuget {

  local packageName=$1

  connect_to_azure_artifact_feed

  local packageVersion=$(head -1 version.txt)
  
  echo "Pushing version: $packageVersion"
  
  dotnet pack -c:Release -p:PackageVersion=$packageVersion

  if [ ! $? -eq 0 ]; then
    echo "ERROR! Unable to create nuget package";
    exit 1;
  fi

  echo "> Publishing to Azure Artifact Feed"
  dotnet nuget push -s artifacts -k key "./bin/Release/$packageName.$packageVersion.nupkg"

  if [ ! $? -eq 0 ]; then
    echo "ERROR! Unable to push to Azure Artifact Feed";
    exit 1;
  fi

  echo "> Bumping version for next push"
  local majorVersion=$(echo "$packageVersion" | cut -d "." -f 1)
  local minorVersion=$(echo "$packageVersion" | cut -d "." -f 2)
  local buildNumber=$(echo "$packageVersion" | cut -d "." -f 3)
  
  buildNumber=$(echo "$(($buildNumber + 1))")
  
  git pull --no-rebase
  
  local newPackageVersion=$majorVersion.$minorVersion.$buildNumber
  echo "Next version: $newPackageVersion"

  echo "${newPackageVersion}" > version.txt
  git add version.txt
  git commit -m "[skip ci] Updating version.txt with latest build number."
  git push
}
