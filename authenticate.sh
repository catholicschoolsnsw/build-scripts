#!/bin/bash

# Validate input data
if [ -z "${ENVIRONMENT}" ]; then
    echo "ERROR: ENVIRONMENT is not defined, Please set environment variable ENVIRONMENT with appropriate value (possible values are dev, qa, prod).";
    exit 1;
fi

if [ -z "${SERVICE_PRINCIPAL_APP_ID}" ]; then
    echo "ERROR: SERVICE PRINCIPAL APP ID is not defined, Please set environment variable SERVICE_PRINCIPAL_APP_ID";
    exit 1;
fi

if [ -z "${SERVICE_PRINCIPAL_SECRET}" ]; then
    echo "ERROR: SERVICE PRINCIPAL SECRET is not defined, Please set environment variable SERVICE_PRINCIPAL_SECRET";
    exit 1;
fi

if [ -z "${AZURE_TENANT_ID}" ]; then
    echo "ERROR: AZURE TENANT ID is not defined, Please set environment variable AZURE_TENANT_ID";
    exit 1;
fi

if [ -z "${SUBSCRIPTION_ID}" ]; then
    echo "ERROR: SUBSCRIPTION ID is not defined, Please set environment variable SUBSCRIPTION_ID";
    exit 1;
fi

if [ -z "${LOCATION}" ]; then
    echo "ERROR: LOCATION is not defined, Please set environment variable LOCATION";
    exit 1;
fi

if [ -z "${NUGET_SOURCE_USERNAME}" ]; then
    echo "ERROR: NUGET SOURCE USERNAME is not defined, Please set environment variable NUGET_SOURCE_USERNAME";
    exit 1;
fi

if [ -z "${NUGET_SOURCE_PASSWORD}" ]; then
    echo "ERROR: NUGET SOURCE PASSWORD is not defined, Please set environment variable NUGET_SOURCE_PASSWORD";
    exit 1;
fi

if [ -z "${NUGET_SOURCE_URL}" ]; then
    echo "ERROR: NUGET SOURCE URL is not defined, Please set environment variable NUGET_SOURCE_URL";
    exit 1;
fi

echo "> Connect to Azure Artifact Feed"
dotnet nuget add source $NUGET_SOURCE_URL -n artifacts -u $NUGET_SOURCE_USERNAME -p $NUGET_SOURCE_PASSWORD --store-password-in-clear-text

echo "> Logging into AZ CLI"
az login --service-principal -u $SERVICE_PRINCIPAL_APP_ID -p $SERVICE_PRINCIPAL_SECRET --tenant $AZURE_TENANT_ID

echo "> Selecting azure account $SUBSCRIPTION_ID";

az account set -s $SUBSCRIPTION_ID

echo "> Logging into PULUMI CLI"
pulumi login  --non-interactive

if pulumi stack select $PULUMI_ORGANISATION_NAME/$ENVIRONMENT ; then
    echo "Pulumi stack $ENVIRONMENT already exists";
else 
    echo "Pulumi stack $ENVIRONMENT does not exist";
    echo "Creating new stack...";
#IG    pulumi stack init --secrets-provider=passphrase $PULUMI_ORGANISATION_NAME/$ENVIRONMENT
    echo "Organisation/Environmnet: $PULUMI_ORGANISATION_NAME/$ENVIRONMENT";
    pulumi stack init $PULUMI_ORGANISATION_NAME/$ENVIRONMENT
fi

echo "> Pulumi config secrets provider";
#IG pulumi stack change-secrets-provider passphrase
pulumi stack change-secrets-provider default
echo "> Pulumi config clientid";
echo "> Service Principal App Id $SERVICE_PRINCIPAL_APP_ID";
echo "> Service Principal Secret $SERVICE_PRINCIPAL_SECRET";
echo "> SubscriptionId $SUBSCRIPTION_ID";
pulumi config set azure:clientId $SERVICE_PRINCIPAL_APP_ID
echo "> Pulumi config client secret";
#IG pulumi config set azure:clientSecret $SERVICE_PRINCIPAL_SECRET --secret
pulumi config set azure:clientSecret $SERVICE_PRINCIPAL_SECRET --plaintext
echo "> Pulumi config tenant";
pulumi config set azure:tenantId $AZURE_TENANT_ID
echo "> Pulumi config subscription";
pulumi config set azure:subscriptionId $SUBSCRIPTION_ID
echo "> Pulumi config location";
pulumi config set azure:location $LOCATION
echo "> Pulumi config azure native clientid";
pulumi config set azure-native:clientId $SERVICE_PRINCIPAL_APP_ID
echo "> Pulumi config azure native client secret";
#pulumi config set azure-native:clientSecret $SERVICE_PRINCIPAL_SECRET --secret
pulumi config set azure-native:clientSecret $SERVICE_PRINCIPAL_SECRET --plaintext
echo "> Pulumi config azure native tenant";
pulumi config set azure-native:tenantId $AZURE_TENANT_ID
echo "> Pulumi config azure native subscription";
pulumi config set azure-native:subscriptionId $SUBSCRIPTION_ID
echo "> Pulumi config azure native location";
pulumi config set azure-native:location $LOCATION

if [ ! -z "${CLOUDFLARE_TOKEN}" ]; then
    echo "> Cloudflare API token";
#IG    pulumi config set cloudflare:apiToken $CLOUDFLARE_TOKEN --secret
    pulumi config set cloudflare:apiToken $CLOUDFLARE_TOKEN --plaintext
fi

# Terraform provider needs the ARM template variable set. https://github.com/pulumi/pulumi/issues/3931
echo "> Pulumi terraform config";
export ARM_CLIENT_ID=$SERVICE_PRINCIPAL_APP_ID
export ARM_CLIENT_SECRET=$SERVICE_PRINCIPAL_SECRET
export ARM_TENANT_ID=$AZURE_TENANT_ID
export ARM_SUBSCRIPTION_ID=$SUBSCRIPTION_ID

echo "> Authentication complete";
