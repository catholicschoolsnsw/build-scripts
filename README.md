# README

## What is this repository for?

-   Utility scripts and templates to aid with CI/CD.

## Contents

### authenticate.sh

-   Validation for required variables
-   Sets up nuget auth
-   Authenticates with Azure CLI and Pulumi
-   Ensures your Pulumi stack exists
-   Configures required pulumi variables

**Ensure you call authenticate.sh from the directory that contains your pulumi project file.**

### destroy-pulumi-stack.sh

-   Validation for required variables
-   Invokes Pulumi destroy and deletes the stack configuration.

**Ensure you call destroy-pulumi-stack.sh from the directory that contains your pulumi project file.**

### install-base-dependencies.sh

-   Installs curl and the Azure CLI.
